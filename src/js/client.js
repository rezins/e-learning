import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {hashHistory, IndexRoute, Route, Router} from "react-router";

//Pages
import Auth from "./pages/auth";
import Home from "./pages/home";
import Layout from "./pages/layout";
import CourseDetail from "./pages/course-detail";
import NowCourse from "./pages/now-course";
import Informatika from "./pages/informatika/informatika";
import Algoritma from "./pages/informatika/semester1/algoritma";

//import Library from "./pages/library";

//Components
import Login from "./components/auth/login";
import Signup from "./components/auth/signup";

import store from "./store";


const app = document.getElementById('app');

ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path='/home' component={Layout}> 
				<IndexRoute component={Home} />
                <Route path="home" name="home" component={Home} />	
                <Route path="coursedetail/:id" name="Detail Konten" component={(props)=> <CourseDetail  {...props} /> }/>
                <Route path="nowcourse/:id" name="Belajar" component={(props)=> <NowCourse  {...props} /> }/>
                <Route path="informatika/informatika" name="informatika" component={Informatika}/>
                <Route path="informatika/semester1/algoritma" name="algoritma" component={Algoritma}/>  		
            </Route>
			<Route path='/' component={Auth}>
                <IndexRoute component={Login} />
                <Route path="login" name="login" component={Login} />
                <Route path="signup" name="signup" component={Signup} />
            </Route>
        </Router>
    </Provider>
    , app);
	
	
	
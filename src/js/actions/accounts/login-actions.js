/**
 * Created by luthfi on 12/20/16.
 */
//Middleware for calling API
import { webStorage } from '../../utils'
import dtiapiClient from '../../dtiapi-client'

import { getAccount } from './get-account-actions'

//Process type for Login
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

function requestLogin(credential) {
    return {
        type: LOGIN_REQUEST,
        isFetching: true,
        isAuthenticated: false,
        credential
    }
}

function receiveLogin(data) {
    return {
        type: LOGIN_SUCCESS,
        isFetching: false,
        isAuthenticated: true,
        token: data.jwt,
        userId: data.user.username,
		account: data.user
    }
}

function loginError(message) {
    return {
        type: LOGIN_FAILURE,
        isFetching: false,
        isAuthenticated: false,
        message
    }
}

export function loginUser(credential) {
    return dispatch => {
        // We dispatch requestLogin to kickoff the call to the API
        dispatch(requestLogin(credential));
          return dtiapiClient.loginAccount(credential)
	      //return svaraClient.loginApp(credential)
            .then(function (response) {
                console.log(response);
                if (response.jwt === undefined) {
                    return Promise.reject(new Error(response.message));
                } else {					
                    webStorage.setItem('token', response.jwt);
                    webStorage.setItem('userId', response.user.username);
					webStorage.setItem('dataUser', response.user); 
										
                    dispatch(receiveLogin(response));
                    dispatch(getAccount(response.user));
                }
            })
            .catch(function (error) {
                dispatch(loginError(error));
                return Promise.reject(error);
            });
    }
}
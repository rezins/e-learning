/**
 * Created by luthfi on 12/20/16.
 */
import {combineReducers} from 'redux'

import account from './account-reducer'

export default combineReducers({
    account
});

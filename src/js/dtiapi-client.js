/**
 * Created by luthfi on 17/04/17.
 */

import axios from 'axios'
import { webStorage } from './utils'

const API_BASE = 'http://genericapis.ligarian.com/';
const API_BASE_LOCAL = 'http://localhost:2337/';

export default {
  loginApp () {
    return new Promise((resolve, reject) => {
      const credential = {
       identifier: 'genericAdmin',
        password: 'passwordAdmin',
      };
      axios.post(`${API_BASE}auth/local`, credential).then(response => {
        resolve(response.data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  loginAccount (credential) {
    return new Promise((resolve, reject) => {
      this.loginApp().then((token) => {
        axios.post(`${API_BASE}auth/local`, credential).then((response) => {
          resolve(response.data)
        }).catch((error) => {
          reject(error)
        })
      }).catch((error) => {
        reject(error)
      })
    })
  },
  registerAccount (account) {
    return new Promise((resolve, reject) => {
      this.loginApp().then((token) => {
        account.userParam = {
          appId: token.appId
        };
        axios.post(`${API_BASE}auth/local/register?jwt=${token.accessToken}`, account).then((response) => {
          if (response.data.id === undefined)
            reject(response.data)
          else
            resolve(response.data)
        }).catch((error) => {
          reject(error)
        })
      }).catch((error) => {
        reject(error)
      })
    })
  },
  apiGet (url, params = {}) {
    let _params = Object.assign(params);
    _params.jwt = webStorage.getItem('token');
    return axios.get(API_BASE + url, {
      params: _params
    })
  },  
   apiGetAuth (url, params = {}) {
    let _params = Object.assign(params);
    _params.jwt = webStorage.getItem('token');
    const AuthStr = 'Bearer '.concat(webStorage.getItem('token'));
   //const AuthStr = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6W10sInRpbWVsaW5lcyI6W10sInByb2R1Y3RzIjpbXSwicGF5bWVudF9oaXN0b3JpZXMiOltdLCJsb2NhdGlvbnMiOltdLCJmaWxlc0NyZWF0ZWQiOltdLCJjb250ZW50Q2F0ZWdvcmllcyI6W10sImNvbnRlbnRzIjpbXSwiYm9va21hcmtzIjpbXSwidHJhbnNhY3Rpb25zIjpbXSwibGlrZSI6W10sImxpa2VzIjpbXSwidXNlcl9jb250ZW50cyI6W10sInVzZXJDb250ZW50cyI6W10sImNvbnRlbnRzTGlicmFyaWVzIjpbXSwiY3JlYXRlZEF0IjoiMjAxNy0wNS0xN1QwODo1NTo0OS42NDNaIiwidGVtcGxhdGUiOiJkZWZhdWx0IiwibGFuZyI6ImVuX1VTIiwidXNlcm5hbWUiOiJ1c2VyIiwiZW1haWwiOiJpemh1cjIwMDFAeWFob28uY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkL1luV29CQXZxdUNzQWt5NGtRbVdTdVhlZEp3aUNoUk5tQmNMNG5mdkFyRVg0SUdpY0VTRlciLCJpZF9yZWYiOiIxIiwicHJvdmlkZXIiOiJsb2NhbCIsImFjY291bnRTdGF0dXMiOiIxIiwidXBkYXRlZEF0IjoiMjAxNy0wNS0xOVQwNDoxNDoxOS41OTJaIiwicmVzZXRQYXNzd29yZFRva2VuIjoiYTBhMDkwMjRjMWZiMzMyMDYzMmI4NTc4Y2Y1NzU4MWQ2ZjMzMTA5ZWJlY2U4NDUzM2E4ZDRiNWU5NzE0NTVkY2EwODE2YmZkMTlmM2MyNjNkZDY3MjBmNmI3YTZmOGY2MTk2ZjZiMjdjNjk0YjIwNDkzMzE3OTE4Mzc1ZGRmZGIiLCJiYWxhbmNlIjoxNzUwMDAsInBvaW50IjpudWxsLCJpZCI6IjU5MWMxMDQ1YjFhNGZjNjgwOWNhYWUwZCIsImlhdCI6MTQ5NTQyNzc3NH0.IB5ZDc7hXvKVvj8e_J3fUtaDDDCollcTRYRlpeYJMZE';
    return axios.get(API_BASE + url, { headers:{ Authorization: AuthStr} 
    })
    
  },  
   apiGetLocal (url, params = {}) {
    let _params = Object.assign(params);
    _params.jwt = webStorage.getItem('token');
    return axios.get(API_BASE_LOCAL + url, {
      params: _params
    })
  },
  apiPost (url, data, config = {}) {
    let _params = {
      jwt: webStorage.getItem('token')
    };
    return axios.post(API_BASE + url, data, {
      params: _params,
      ...config
    })
  }, 
  apiPut (url, data) {
    let _params = {
      jwt: webStorage.getItem('token')
    };
    return axios.put(API_BASE + url, data, {
      params: _params
    })
  },
  apiDelete (url) {
    let _params = {
      jwt: webStorage.getItem('token')
    };
    return axios.delete(API_BASE + url, {
      params: _params
    })
  },
  apiHead (url, params = {}) {
    let _params = Object.assign(params);
    _params.jwt = webStorage.getItem('token');
    return axios.head(API_BASE + url, {
      params: _params
    })
  },
  getAudioUrl (audio) {
    const re = new RegExp(/(http\:\/\/)|(https\:\/\/)/);
    if (re.test(audio))
      return audio;
    else
      return `${API_BASE}${audio}?jwt=${webStorage.getItem('token')}`;
  }
}
/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { hashHistory } from 'react-router'
import {Link} from 'react-router'
import { connect } from 'react-redux'
import { logoutAccount } from '../../actions/accounts/logout-actions';
import { loginUser } from '../../actions/accounts/login-actions'

export default class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            error: null
        }

    }

    componentDidMount() {

    }



    handleLogout(e) {
        e.preventDefault();
        const { dispatch, isAuthenticated } = this.props;
        dispatch(logoutAccount());
        hashHistory.push('/');
    }

    handleSubmit(event) {
        event.preventDefault();
        const username = event.target.username.value;
        const password = event.target.password.value;
        const { isAuthenticated, dispatch } = this.props;

        const self = this;

        this.setState({
            isLoading: true
        });

        let credential = {};
        if (this.validateEmail(username))
            credential.email = username;
        else
            credential.identifier = username;

        credential.password = password;

        dispatch(loginUser(credential)).then(() => {
            if (isAuthenticated) {
                hashHistory.push('/');

            }



        }).catch((error) => {
            self.setState({
                isLoading: false,
                error: error
            })
        })
    }

    validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    render() {
        const { isAuthenticated, dataUser, userId } = this.props;
        const { error } = this.state;
        let errorMessage = '';
        if (error)
            errorMessage = error.message;

        var headerIcon;
        if (isAuthenticated) {
            headerIcon = (
                  <div id="header-top-wrap">
                    <section id="header-top" className="clearfix">
                        <p className="contact">
                            E-Learning UNJANI
                        </p>
                        <p className="login">
                           AEK MUSTOFA 
                         <a href="#" onClick={this.handleLogout.bind(this)}><i className="icon-log-out"></i> Logout</a>
                            
                        </p>

                    </section>
                </div>

            )
        } else {
            headerIcon = (
                 <div id="header-top-wrap">
                    <section id="header-top" className="clearfix">
                        <p className="contact">
                            E-Learning UNJANI
                        </p>
                        <p className="login">
                            Welcome visitor, if you have an account
                        <a href="#/auth"> Login</a>, or
                        <a href=""> Register</a>
                       
                        </p>

                    </section>
                </div>
            )
        }
        return (
            <header id="header">
               {headerIcon}
                <div id="header-bottom-wrap">
                    <section id="header-bottom">
                        <div className="logo-container">
                            <a href="#/home" >
                                <figure className="logo">
                                    <img src="/assets2/images/unjani.png" alt="logo"></img>
                                    <figcaption>Westeros</figcaption>
                                </figure>
                            </a>
                        </div>
                        <form className="westeros-form">
                            <label for="categories" className="select-block">
                                <select name="categories" id="categories">
                                    <option value="0">All Fakultas</option>
                                    <option value="1">Fakultas Kedokteran</option>
                                    <option value="2">Fakultas MIPA</option>
                                    <option value="3">Fakultas Ekonomi</option>
                                    <option value="4">Fakultas Teknik</option>
                                    <option value="5">Fakultas Psikologi</option>
                                    <option value="6">Fakultas ISIP</option>
                                    <option value="7">Fakultas Farmasi</option>
                                </select>
                                <svg className="svg-arrow select-arrow">
                                    <use xlinkHref="#svg-arrow"></use>
                                </svg>
                            </label>
                            <input type="text" name="search" id="search" placeholder="Search Our Catalog..."></input>
                            <input type="image" src="/assets2/images/icons/search-icon.png" alt="search-icon"></input>
                        </form>
                    </section>
                </div>
                <ul className="westeros-separator small">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <nav id="main-menu">
                    <img className="pull-nav" src="images/icons/pull-icon.png" alt="pull-icon"> </img>
                    <ul>
                        <li>
                            <a href="" className="submenu">F KEDOKTERAN</a>

                            <ul className="submenu-small">
                                <li>
                                {/* <li><a href="#" onClick={this.handleLogout.bind(this)}><i className="icon-log-out"></i> Logout</a></li> */}
                                    <a href="register-login.html">KEDOKTERTAN UMUM</a>
                                </li>
                                <li>
                                    <a href="register-login.html">KEDOKTERAN GIGI</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="" className="submenu">F MIPA</a>
                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">KIMIA</a>
                                </li>
                                <li>
                                <Link to="home/informatika/informatika">INFORMATIKA</Link>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="" className="submenu">F EKONOMI</a>
                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">AKUTANSI</a>
                                </li>
                                <li>
                                    <a href="register-login.html">MANAJEMEN</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="" className="submenu">F TEKNIK</a>
                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">TEKNIK MESIN</a>
                                </li>
                                <li>
                                    <a href="register-login.html">TEKNIK ELEKTRO</a>
                                </li>
                                <li>
                                    <a href="register-login.html">TEKNIK METALURGI</a>
                                </li>
                                <li>
                                    <a href="register-login.html">TEKNIK KIMIA</a>
                                </li>
                                <li>
                                    <a href="register-login.html">TEKNIK INDUSTRI</a>
                                </li>
                                <li>
                                    <a href="register-login.html">TEKNIK SIPIL</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="" className="submenu">F PSIKOLOGI</a>

                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">PSIKOLOGI</a>
                                </li>
                            </ul>

                        </li>
                        <li>
                            <a href="" className="submenu">FISIP</a>

                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">ILMU PEMERINTAHAN</a>
                                </li>
                                <li>
                                    <a href="profile.html">HUBUNGAN INTERNASIONAL</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="" className="submenu">F FARMASI</a>
                            <ul className="submenu-small">
                                <li>
                                    <a href="register-login.html">FARMASI</a>
                                </li>
                                <li>
                                    <a href="profile.html">APOTEKER</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </nav>
                
            </header >
        );
    }
}

/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'
import {logoutAccount} from '../../actions/accounts/logout-actions';

export default class Header extends React.Component {
    constructor() {
        super();
    }
	
	handleLogout(e) {
		e.preventDefault();
		const {dispatch, isAuthenticated} = this.props;
		dispatch(logoutAccount());		
		hashHistory.push('/auth');		 
    }

	
	render() {
        const { account } = this.props;
        return (		
	<header id="header" className=""> 
		<div className="top-bar">
            <div className="container">
                <div className="row">
				
				<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<ul className="top-nav nav-left">
                            <li><a href="#">Home</a></li>
				</ul>
				{/*
                        <ul className="top-nav nav-left">
                            <li><a href="#">Students</a></li>
                            <li><a href="#">Faculty &amp; Staff</a></li>
                            <li><a href="#">Parents</a></li>
                            <li><a href="#">Alumni</a></li>
                        </ul>
						*/}
                    </div>
				
                    <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    	<div className="cs-user">
                            <ul>
                                <li><a href="/#/auth"><i className="icon-login"></i>Login</a></li>
                                <li><a href="#/auth/signup" ><i className="icon-user2"></i>Signup</a></li>
								
								
                                <li>
                                    <div className="cs-user-login">
                                        <div className="cs-media">
                                            <figure><img alt="" src="assets/images/faiza.jpg"></img></figure>
                                        </div>
                                        <a href="#">Faiza Rhenaldi</a>
                                        <ul>
                                            <li><a href="user-detail.html"><i className="icon-user3"></i> About me</a></li>
                                            <li><a href="user-courses.html"><i className="icon-graduation-cap"></i> My Courses</a></li>
                                            <li><a href="user-short-listed.html"><i className="icon-heart"></i> Favorites</a></li>
                                            <li><a href="user-statements.html"><i className="icon-text-document"></i> Statement</a></li>
                                            <li className="active"><a href="user-account-setting.html"><i className="icon-gear"></i> Profile Setting</a></li>
                                            <li><a href="#" onClick={this.handleLogout.bind(this)}><i className="icon-log-out"></i> Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
								
								
                            </ul>
                        </div>
                    {/*    						
                        <ul className="top-nav nav-right">
                            <li><a href="#">APPLY</a></li>
                            <li><a href="#">PROGRAMS &amp; DEGREES</a></li>
                            <li><a href="#">FIND FUNDINg</a></li> 
                        </ul>
					*/}
                    </div>
                </div>
            </div>
		</div>
		
				
        <div className="main-header">
            <div className="container">
                <div className="row">
                    <div className="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                        <div className="cs-logo cs-logo-dark">
                            <div className="cs-media">
                                <figure><a href="index-2.html"><img src="assets/images/cs-logo.png" alt=""/></a></figure>
                            </div>
                        </div>
                        <div className="cs-logo cs-logo-light">
                            <div className="cs-media">
                                <figure><a href="index-2.html"><img src="assets/images/xcs-logo-light.png.pagespeed.ic.Q1HdweYLsy.png" alt=""/></a></figure>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                        <div className="cs-main-nav pull-right">
                            <nav className="main-navigation">
                                <ul>
                                    <li><a href="index-2.html">Profil</a><span>Tentang DTI</span></li>
                                    <li className="menu-item-has-children"><a href="#">Program</a>
                                        <span>Online Education</span>
                                        <ul>
                                            <li><a href="courses-grid.html">Courses grid view</a></li>
                                            <li><a href="courses-simple.html">Courses Simple view</a></li>
                                            <li><a href="courses-listing.html">Courses list view</a></li>
                                            <li><a href="cs-courses-detail.html">Courses Detail</a></li>
                                        </ul>
                                    </li>
                                    <li className="menu-item-has-children"><a href="#">Konten</a><span>All you need</span>
                                    	<ul>
                                            <li><a href="user-detail.html">Motivasi Harian</a></li>
                                            <li><a href="instructor-detail.html">Kata Bijak</a></li>
								    <li><a href="affiliations.html">Video</a></li>
                                        	<li><a href="typography.html">Audio</a></li>
                                            <li className="menu-item-has-children"><a href="shortcode.html">Ceramah</a>
												<ul>
													<li><a href="loop.html">Kuliah Subuh</a></li>
												
													<li><a href="loop.html">Kuliah Dhuha</a></li>
												</ul>
											</li>
                                            <li><a href="about-us.html">Manajemen Qalbu</a></li>
                                            <li><a href="faqs.html">Jagalah Hati</a></li>
											{/*
                                            <li><a href="under-construction.html">Maintenance Page</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                            <li><a href="signup.html">Signup / Login</a></li>
                                            <li><a href="pricing.html">Price Table</a></li>
                                        	<li className="menu-item-has-children"><a href="#">Team</a>
                                            	<ul>
                                                    <li><a href="team-listing.html"> Team List</a></li>
                                                    <li><a href="team-grid.html"> Team Grid</a></li>
                                                    <li><a href="team-detail.html"> Team Detail</a></li>
                                                </ul>
                                            </li>
                                            
                                            <li className="menu-item-has-children"><a href="#">Shop</a>
                                            	<ul>
                                                    <li><a href="shop.html"> Products</a></li>
                                                    <li><a href="shop-detail.html"> Detail</a></li>
                                                </ul>
                                            </li>
											*/}
                                        </ul>
                                    </li>
                                    <li className="menu-item-has-children"><a href="#">Events</a><span>Agenda Kegiatan</span>
                                    	<ul>
                                        	<li><a href="events-grid.html">Kegiatan Rutin</a></li>
                                            <li><a href="events-listing.html">Pelatihan View</a></li>
                                            <li><a href="events-detail.html">Seminar</a></li>
                                        </ul>
                                    </li>
									{/*
                                    <li className="menu-item-has-children"><a href="#">Blog</a><span>Learning Objectives</span>
                                    	<ul>
                                        	<li><a href="blog-medium.html">Medium List</a></li>
                                            <li><a href="blog-large.html">Large List</a></li>
                                             <li><a href="blog-grid.html">Grid</a></li>
                                            <li><a href="blog-detail.html">Detail</a></li>
											<li><a href="blog-2.html">Masonry</a></li>
                                        </ul>
                                    </li>
									*/}
                                    <li className="menu-item-has-children"><a href="#">Kontak</a><span>inquire with us</span>
                                    	<ul>
                                            <li><a href="contact-us.html">Hubungi Kami</a></li>
                                            <li><a href="contact-us-02.html">Lokasi Kami</a></li>
                                        </ul>
                                    </li>
											
                                    <li className="cs-search-area">
									
                                        <div className="cs-menu-slide">
                                            <div className="mm-toggle">
                                                <i className="icon-align-justify"></i>
                                            </div>            
                                        </div>
										
                                        <div className="search-area">
                                            <a href="#"><i className="icon-search2"></i></a>
                                            <form>
                                                <div className="input-holder">
                                                    <i className="icon-search2"></i>
                                                    <input type="text" placeholder="Enter any keyword"></input>
                                                    <label className="cs-bgcolor">
                                                        <i className="icon-search5"></i>
                                                        <input type="submit" value="search"></input>
                                                    </label>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                            <div className="cs-search-area hidden-md hidden-lg">
                                <div className="cs-menu-slide">
                                    <div className="mm-toggle">
                                        <i className="icon-align-justify"></i>
                                    </div>            
                                </div>
                                <div className="search-area">
                                    <a href="#"><i className="icon-search2"></i></a>
                                    <form>
                                        <div className="input-holder">
                                            <i className="icon-search2"></i>
                                            <input type="text" placeholder="Enter any keyword"></input>
                                            <label className="cs-bgcolor">
                                                <i className="icon-search5"></i>
                                                <input type="submit" value="search"></input>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</header>
		
		       );
    }
}

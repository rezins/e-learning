/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'
import {logoutAccount} from '../../actions/accounts/logout-actions';

export default class LoginInfo extends React.Component {
    constructor() {
        super();
    }
	
	handleLogout(e) {
		e.preventDefault();
		const {dispatch, isAuthenticated} = this.props;
		dispatch(logoutAccount());		
		hashHistory.push('/auth');		 
    }
	
	render() {
      const {dispatch, isAuthenticated,account } = this.props;
	  let UserProfile="";
	  if(!isAuthenticated){ 
        UserProfile= (	
                              
                                    <div className="cs-user-login">
                                        <div className="cs-media">
                                            <figure><img alt="" src="assets/images/faiza.jpg"></img></figure>
                                        </div>
                                        <a href="#">Faiza Rhenaldi</a>
                                        <ul>
                                            <li><a href="user-detail.html"><i className="icon-user3"></i> About me</a></li>
                                            <li><a href="user-courses.html"><i className="icon-graduation-cap"></i> My Courses</a></li>
                                            <li><a href="user-short-listed.html"><i className="icon-heart"></i> Favorites</a></li>
                                            <li><a href="user-statements.html"><i className="icon-text-document"></i> Statement</a></li>
                                            <li className="active"><a href="user-account-setting.html"><i className="icon-gear"></i> Profile Setting</a></li>
                                            <li><a href="#" onClick={this.handleLogout.bind(this)}><i className="icon-log-out"></i> Logout</a></li>
                                        </ul>
                                    </div>
                               		
		       )
	    }
		else{
			UserProfile=(
          <div>Not Login</div>		
        )
	    }

		return(
		  <li>
		  {UserProfile}
		  </li>
		);	
    }
}

/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'

export default class Footer extends React.Component {
    constructor() {
        super();
    }	
	render() {
        return (
		<footer>
			<div id="footer-bottom-wrap">
				<section id="footer-bottom">
					<p><a href="index.html">E-Learning UNJANI</a> <span>|</span> All Rights Reserved 2017</p>
					<ul className="social-links">
						<li className="fb"><a href="#"></a></li>
						<li className="twt"><a href="#"></a></li>
						<li className="gplus"><a href="#"></a></li>
						<li className="db"><a href="#"></a></li>
						<li className="rss"><a href="#"></a></li>
						<li className="vm"><a href="#"></a></li>
						<li className="fk"><a href="#"></a></li>
					</ul>
				</section>
			</div>
		</footer>
		       );
    }
}

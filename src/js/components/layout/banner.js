/**
 * Created by Abahsoft on 19/05/2017.
 */
import React from 'react'
import { IndexLink, Link } from 'react-router'
import { connect } from 'react-redux'
import dtiapiClient from '../../dtiapi-client'

export default class BannerTop extends React.Component {
	constructor() {
		super();
	}

	render() {
		const {account,isAuthenticated, location } = this.props;
        
        return (
			<div>
				<div id="banner-wrap">
					<section id="banner">
						<div className="main-promo">
							<h2>Selamat Datang di E-Learning</h2>
							<p>Situs ini merupakan situs E-learning UNJANI di mana seluruh mahasiswa UNJANI dapat mengakses Video Pembelajaran setiap jurusan</p>
						</div>
					</section>
				</div>
				
			</div>

		);
	}
}

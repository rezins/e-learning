/**
 * Created by luthfi on 12/17/16.
 */
import React from 'react'
var playQueue;
export default class Player extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        $('#player-bottom').hide();
        playQueue = new jPlayerPlaylist({
            jPlayer: "#jplayer_N",
            cssSelectorAncestor: "#jp_container_N"
        }, [], {
            playlistOptions: {
                enableRemoveControls: true,
                autoPlay: false
            },
            swfPath: "https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.swf",
            solution: "html, flash",
            supplied: "webmv, ogv, m4v, oga, mp3",
            smoothPlayBar: false,
            keyEnabled: true,
            audioFullScreen: false
        });
        window.currentSong = null;
        window.playing = false;
        $("#jplayer_N").bind($.jPlayer.event.play, function(event) {

            $('#player-bottom').show('slow');

            $('body').css('padding-bottom', '60px');

            window.itemId = event.jPlayer.status.media.id;
            window.sourceId = event.jPlayer.status.media.sourceId;

            if (window.currentSong != null) {
                if (window.currentSong.media.mp3 === event.jPlayer.status.media.mp3) {
                    $("#jplayer_N").jPlayer('play', window.currentSong.currentTime);
                }
                window.currentSong = null;
            }

            $('#jp-poster').attr('src', event.jPlayer.status.media.poster);
            $('.song-title').html(event.jPlayer.status.media.title);
            $('#song-artist').html(event.jPlayer.status.media.artist);
            $('.jp-play').removeClass('app-show').addClass('app-hide');
            $('.jp-pause').removeClass('app-hide').addClass('app-show');
            window.playing = true;
        });

        $("#jplayer_N").bind($.jPlayer.event.pause, function(event) {
            $('.jp-pause').removeClass('app-show').addClass('app-hide');
            $('.jp-play').removeClass('app-hide').addClass('app-show');
            window.currentSong = {
                currentTime: event.jPlayer.status.currentTime,
                media: event.jPlayer.status.media
            };
            window.playing = false;
        });

        $("#jplayer_N").bind($.jPlayer.event.seeked, function(event) {

            var width = $('.jp-seek-bar').width();
            var currentWidth = $('.jp-play-bar').width();

            var curentTime = (currentWidth/width) * event.jPlayer.status.currentTime;

            window.currentSong = {
                currentTime: event.jPlayer.status.currentTime,
                media: event.jPlayer.status.media
            };
            if (window.playing) {
                window.playQueue.play();
            }
        });

        $('.jp-play').click(function (e) {
            e.preventDefault();
            window.playing = true;
        });

        $('.jp-pause').click(function (e) {
            e.preventDefault();
            window.playing = false;
        });

        window.playQueue = playQueue;

        setInterval(function () {
            $('.item .item-overlay').removeClass('active');
            $('.item .item-overlay .item-action').removeClass('active');
            $('.playlist-track').removeClass('active');
            $('.artist-track').removeClass('active');
            $('.playlist-track .track-play').removeClass('active');
            $('.'+window.itemId).addClass('active');
            $('.item-library.'+window.sourceId).addClass('active');
            $('.btn-control-playlist').html('play');
            if (window.playing) {
                $('.'+window.sourceId).addClass('active');
                $('.'+window.sourceId + ' .item-action').addClass('active');
                $('.'+window.itemId + ' .item-action').addClass('active');
                $('.'+window.itemId + ' .track-play').addClass('active');
                $('.btn-control-playlist'+'.'+window.sourceId).html('pause');
            }
        }, 100);
    }

    addSong(song) {
        playQueue.add(song);
    }

    ifSideNavOpen(open) {
        if (open)
            return ' hidden-xs';
        else
            return '';
    }

    render() {
        return (
            <footer id="player-bottom" className={"footer bg-dark bg-charcoal-grey" + this.ifSideNavOpen(this.props.navMobileOpen)}>
                <div className="player-song-info-xs hidden-sm hidden-lg hidden-md">
                    <div className="wrapper-sm clearfix">
                        <span className="clear">
                            <span className="block m-l">
                                <strong className="text-ellipsis font-bold text-lt song-title">Title</strong>
                            </span>
                        </span>
                    </div>
                </div>
                <div id="jp_container_N">
                    <div className="jp-type-playlist">
                        <div id="jplayer_N" className="jp-jplayer hide"></div>
                        <div className="jp-gui">
                            <div className="jp-video-play hide">
                                <a className="jp-video-play-icon">play</a>
                            </div>
                            <div className="jp-interface">
                                <div className="jp-controls">
                                    <div className="jp-poster">
                                        <span className="thumb-sm pull-left m-l-xs">
                                            <img id="jp-poster" src="assets/images/album.png" alt=""/>
                                        </span>
                                    </div>
                                    <div className="player-song-info hidden-xs">
                                        <div className="wrapper-sm clearfix">
                                            <span className="clear">
                                                <span className="block m-l">
                                                    <strong className="text-ellipsis font-bold text-lt song-title">Title</strong>
                                                </span>
                                                <span className="text-ellipsis text-muted text-xs block m-l hidden-xs" id="song-artist">Artist</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div><a className="jp-previous"><i className="icon-control-rewind i-lg"></i></a></div>
                                    <div>
                                        <a className="jp-play"><i className="icon-control-play i-2x"></i></a>
                                        <a className="jp-pause hid"><i className="icon-control-pause i-2x"></i></a>
                                    </div>
                                    <div><a className="jp-next"><i className="icon-control-forward i-lg"></i></a></div>
                                    <div className="hide"><a className="jp-stop"><i className="fa fa-stop"></i></a></div>
                                    <div className="hidden-xs jp-current-time text-xs text-muted"></div>
                                    <div className="jp-progress">
                                        <div className="jp-seek-bar dk">
                                            <div className="jp-play-bar bg-gradient">
                                            </div>
                                        </div>
                                    </div>
                                    <div className="jp-duration text-xs text-muted hidden-xs"></div>
                                    <div className="hidden-xs hidden-sm">
                                        <a className="jp-mute" title="mute"><i className="icon-volume-2"></i></a>
                                        <a className="jp-unmute hid" title="unmute"><i className="icon-volume-off"></i></a>
                                    </div>
                                    <div className="jp-volume hidden-xs hidden-sm">
                                        <div className="jp-volume-bar dk">
                                            <div className="jp-volume-bar-value lter"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <a className="jp-shuffle" title="shuffle"><i className="icon-shuffle text-muted"></i></a>
                                        <a className="jp-shuffle-off hid" title="shuffle off"><i className="icon-shuffle text-lt"></i></a>
                                    </div>
                                    <div>
                                        <a className="jp-repeat" title="repeat"><i className="icon-loop text-muted"></i></a>
                                        <a className="jp-repeat-off hid" title="repeat off"><i className="icon-loop text-lt"></i></a>
                                    </div>
                                    <div><a className="" data-toggle="dropdown" data-target="#playlist"><i className="icon-list"></i></a></div>
                                    <div className="hide">
                                        <a className="jp-full-screen" title="full screen"><i className="fa fa-expand"></i></a>
                                        <a className="jp-restore-screen" title="restore screen"><i className="fa fa-compress text-lt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="jp-playlist dropup" id="playlist">
                            <ul className="dropdown-menu aside-xl dker">
                                <li className="list-group-item"></li>
                            </ul>
                        </div>
                        <div className="jp-no-solution hide">
                            <span>Update Required</span>
                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
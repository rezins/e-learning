    /**
 * Created by ical on 25/3/17.
 */

import React from 'react'
import dtiapiClient from '../../dtiapi-client'
import TimeAgo from 'react-timeago'
import { Link } from 'react-router'
import { webStorage, API_BASE, getImageUrl } from '../../utils'

export default class Post extends React.Component {
    constructor(/*props*/) {
        super(/*props*/);
        /*this.postItem = {
			contTitle: this.props.contTitle,
            contFull: this.props.contFull,
            contType: this.props.contType,
			price: this.props.price,
            id: this.props.id
        }*/
    }
	
	renderType(){		 
		if(this.props.cont_type=="video")
		{
			return (
			   <div className="cs-media">	
					<video width="320" height="240" controls controlsList="noDownload">
						<source src={this.props.cont_full} type="video/mp4"></source>
								Your browser does not support the video tag.
						</video> 
				</div>
			)
			
		}
		else if(this.props.cont_type=="audio")
		{
			return (
			   <div className="cs-media">
				<figure><a href="#"><img src="assets/images/audio-default.jpg" alt="" height="180"/>
				</a></figure>			   
					<audio width="320" height="240" controls controlsList="noDownload">
						<source src={this.props.cont_full} type="video/mp4"></source>
								Your browser does not support the video tag.
						</audio> 
				</div>
			)
			
		}
		else if(this.props.cont_type=="image")
		{
			return (
			   <div className="cs-media">
				<a href={this.props.cont_full} target="_blank"><img src={this.props.cont_full} alt="" width="300px" height="300px" ></img>
				</a>			   
				</div>
			)
			
		}
		else{
			return(
				<div className="cs-media">
            		<figure><a href="#"><img src="assets/images/default.jpg" alt=""/></a></figure>
            	</div>

			)	
		}		
	}

    render() {
    	// let linkDetilCource = '/#/coursedetail/' + this.props.id ;
    	//console.log('post.js');
    	//console.log(this.props);
    	var urlThumbnail:String;
    	if(!this.props.thumb) urlThumbnail="assets/images/default.jpg";
    	else urlThumbnail = this.props.thumb

    	var star = [];
		for(var i=0; i<this.props.point;i++){
			star.push(
				<li className="filled">
					<svg className="svg-star">
						<use xlinkHref="#svg-star"></use>
					</svg>
				</li>
			);
		}
        
        return (
			<li className="list-item">
				<div className="actions">
					<figure>
						<img height="300px" width="auto" src={urlThumbnail} alt="product1"></img>
					</figure>				 
				</div>

				<div className="description">
					<div className="clearfix">
						<a href="#"><p className="highlighted category">by INFORMATIKA</p></a>
						<ul className="rating">
							{star}
						</ul>
					</div>

					<div className="clearfix">
						<h6 style={{color:'black'}}>
							<Link style={{color:'black'}}  to={{pathname:'home/coursedetail/'+this.props.id}} >
								{this.props.cont_title}
							</Link>
						</h6>
					</div>
									
					<div className="clearfix">
						<a href="#"><h7 style={{color:'#555'}}>{this.props.total_duration}</h7></a>
					</div>
					
		              <div className="cart-options">
 						<Link className="button login" to={{pathname:'home/coursedetail/'+this.props.id}} >
		               Lihat Detail
		              </Link>
					</div>
				</div>
			</li>
        );
    }
}

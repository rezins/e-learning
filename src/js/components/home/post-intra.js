    /**
 * Created by ical on 25/3/17.
 */

import React from 'react'
import dtiapiClient from '../../dtiapi-client'
import TimeAgo from 'react-timeago'
import { Link } from 'react-router';

import { webStorage, API_BASE, getImageUrl } from '../../utils'

export default class Post extends React.Component {
    constructor(props) {
        super(props);
        this.postItem = {
			contTitle: this.props.contTitle,
            contFull: this.props.contFull,
            contType: this.props.contType,
			price: this.props.price,
            id: this.props.id
        }
    }
	
    renderType(){		 
		if(this.props.cont_type=="video")
		{
			return (
			   <div className="cs-media">	
					<video width="320" height="240" controls controlsList="noDownload">
						<source src={this.props.contFull} type="video/mp4"></source>
								Your browser does not support the video tag.
						</video> 
				</div>
			)
			
		}
		else if(this.props.cont_type=="audio")
		{
			return (
			   <div className="cs-media">
				<figure><a href="#"><img src="assets/images/audio-default.jpg" alt="" height="180"/>
				</a></figure>			   
					<audio width="320" height="240" controls controlsList="noDownload">
						<source src={this.props.cont_full} type="video/mp4"></source>
								Your browser does not support the video tag.
						</audio> 
				</div>
			)
			
		}
		else if(this.props.cont_type=="image")
		{
			return (
			   <div className="cs-media">
				<a href={this.props.cont_full} target="_blank"><img src={this.props.cont_full} alt="" width="300px" height="300px" ></img>
				</a>			   
				</div>
			)
			
		}
		
		
		else{
			return(
				<div className="cs-media">
            		<figure><a href="#"><img src="assets/images/default.jpg" alt=""/></a></figure>
            	</div>

			)	
		}		
	}	
	
    render() {
        return (
				<div>	
				    {this.renderType()}
            		<div className="cs-text">
						<div className="cs-post-title">
						  <h2><a href="#">{this.props.cont_title}</a></h2>
						</div>
						<div className="cs-price-sec">
						 <span className="cs-courses-price"><em>Rp</em>{this.props.price}</span>
						 <div className="cs-rating">
            		      <div className="cs-rating-star">
            		        <span className="rating-box" style={{width:"100%"}}></span>
            		      </div>
            		      <em>4.5</em>
            		     </div>
						</div>
						<div className="cs-post-meta">
						  <span className="post-by">By:
						    <a href="#" className="cs-color">{this.props.createdBy.username}</a>
						  </span>
						</div>
						<p>{this.props.contFull}</p>
						<div className="cs-post-options">
						  <span><span className="cs-values"><em></em><em></em></span>Intermediat</span>
						  <span><i className="icon-uniF117"></i>{this.props.point} Students</span>
						  <span><i className="icon-clock5"></i>1h 41m <span className="cs-caption">{this.props.contType}</span></span>
						</div>
					</div>
				</div>
        );
    }
}

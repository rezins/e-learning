/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'

export default class CourseList extends React.Component {
    constructor() {
        super();
    }
	
	render() {
       
        return (
      	<div className="page-section" style={{background:'#f9fafa',paddingTop:'62px',marginBottom:'82px'}}>
			<div className="container">
			  <div className="row">
			  	<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div className="cs-section-title left">
					  <h2>Program dan Pelatihan</h2>
					  <p style={{color:'#aaaaaa !important'}}>Anda dapat mengikuti berbagai program dan pelatihan menarik</p>
					</div>
              	</div>
				
				<aside className="section-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
              	<div className="widget cs-widget-links">
                	<ul>
                    	<li><a href="#">MQ Spiritual</a></li>
                        <li><a href="#">Work Ethos Motivation</a></li>
                        <li><a href="#">Coorporate Values</a></li>
                        <li><a href="#">Character Building</a></li>
                        <li><a href="#">Leadership</a></li>
                        <li><a href="#">Services Excellence</a></li>
						<li><a href="#">Entreprenuership</a></li>
                        <li><a href="#">Pre-Retirement</a></li>
                        
                    </ul>              
                </div>
              </aside>
				
				<div className="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
				  <div className="row">
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="assets//images/program-pelatihan-masa-persiapan-pensiun-1.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<span className="cs-caption">Free</span>
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box" style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Pelatihan Masa Persiapan Pensiun</a></h5>
								</div>
								<span className="cs-courses-price"><em>Rp. </em>0.0</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">AA </a>
									<a href="#" className="cs-color">Agym</a>
								  </span>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="assets/images/program-pelatihan-wirausaha-muda-15.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box"  style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Pelatihan Wirausaha Muda</a></h5>
								</div>
								<span className="cs-courses-price"><em>Rp. </em>10.000</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">AA </a>
									<a href="#" className="cs-color">Agym</a>
								  </span>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="assets/images/program-pelatihan-tim-prima-7.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box" style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Pelatihan Pembangunan Tim Efektif</a></h5>
								</div>
								<span className="cs-courses-price"><em>Rp. </em>25.000.000</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">Budi Prayitno</a>
									 
								  </span>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="/assets/images/program-pelatihan-mq-super-camp-17.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box" style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Pelatihan MQ Super Camp</a></h5>
								</div>
								<span className="cs-courses-price"><em>Rp. </em>15.000.000</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">Budi Prayitno</a>
									 
								  </span>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="assets/images/keluarga.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<span className="cs-caption">Free</span>
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box" style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Pribadi mempesona</a></h5>
								</div>
								<span className="cs-courses-price"><em>Rp. </em>155.99</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">Faiza Rhenaldi</a>
								  </span>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div className="cs-courses courses-grid">
							<div className="cs-media">
								<figure><a href="#"><img src="assets/images/diklatsar.jpg" alt=""/></a></figure>
							</div>
							<div className="cs-text">
								<div className="cs-rating">
								  <div className="cs-rating-star">
									<span className="rating-box" style={{width:"100%"}}></span>
								  </div>
								</div>
								<div className="cs-post-title">
								  <h5><a href="#">Diklat SAR Manajemen Qalbu</a></h5>
								</div>
								<span className="cs-free">Free</span>
								<div className="cs-post-meta">
								  <span>By
									<a href="#" className="cs-color">M. Ikram</a>
									 
								  </span>
								</div>
							</div>
						</div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		</div>
		       );
    }
}

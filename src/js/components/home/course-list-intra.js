/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'

import { webStorage, API_BASE } from '../../utils'
import dtiapiClient from '../../dtiapi-client'

import Post from './post-intra'


export default class CourseListIntra extends React.Component {
    constructor() {
        super();
		this.state = {
            title: '',
            lastRetrieved: null,
            hasNext: true,
            contents: []
        }
    }
	
	componentWillMount() {
       var self = this;
	      dtiapiClient.apiGet(`contentsPublic`).then(function (response) {
            console.log(response.data);
           self.setState({
               contents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });
    }
	
	
	render() {
		
		var self = this;

        const { hasNext, contents } = self.state;
        const { location } = self.props;
		
		 let items = self.state.contents.map((post) => (
             <li className="col-lg-12 col-md-12 col-sm-12 col-xs-12">       
                <Post 
                    {...post}
					location={location}
                />
			</li>			
        ));
				
        var loader = <div className="loader"></div>;
		  
        return (
      	
		<div className="page-section">
			<div className="container">
				<div className="row">
			    <div className="page-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
	          <div className="row">
	            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	              <div className="cs-sorting-list">
	               	<div className="cs-left-side">
	               	  <div className="cs-select-holder">
	               	    <select className="chosen-select" tabindex="5">
						  <option>Select Topic</option>
						  <option>Select Topic</option>
						  <option>Select Topic</option>
						  <option>Select Topic</option>
						</select>
	               	  </div>
	               	  <ul className="cs-package-list">
	               	  	<li><a href="#">All</a></li>
	               	  	<li><a href="#">Free</a></li>
	               	  	<li><a href="#">Paid</a></li>
	               	  </ul>
	               	  <div className="cs-caption-select">
	               	      <span className="cs-caption">CC</span>
	               	      <input name="name" id="checkboxone" type="checkbox" value="Speed"></input>
						  <label for="checkboxone">Captions</label>
	               	  </div>
	               	</div>
	               	<ul className="cs-list-view">
	               		<li><a href="#"><i className="icon-sweden"></i></a></li>
	               		<li><a href="#"><i className="icon-view_module"></i></a></li>
	               		<li><a href="#"><i className="icon-view_headline"></i></a></li>
	               	</ul>
	              </div>
	            </div>
				
	            <ul className="cs-courses courses-listing">
				
				{items}
	              
	            </ul>
	            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div className="cs-pagination">
						<ul className="pagination">
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">7</a></li>
							<li><a href="#">8</a></li>
							<li><a href="#"><i className=" icon-dots-three-horizontal"></i></a></li>
							<li><a href="#">10</a></li>
							<li><a href="#">36</a></li>
						</ul>
					</div>
				</div>
	          </div>
	        </div>
		
		
		
				</div>
			</div>
		</div>
		       );
    }
}

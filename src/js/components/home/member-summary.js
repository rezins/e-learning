/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'

export default class MemberList extends React.Component {
    constructor() {
        super();
    }
	
	render() {
       
        return (
      		<div className="page-section" style={{backgroundColor:'#f9fafa',padding:'50px 0'}}>
            <div className="container">
                <div className="row">
                    <div className="section-fullwidtht col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul className="row cs-counter-holder">
                            <li className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div className="cs-counter simple center">
                                    <div className="cs-text">
                                        <strong className="counter cs-color">756.254</strong>
                                        <span>Pengguna Aktif</span>
                                        <p>Jumlah pengguna aplikasi dari berbagai belahan dunia.</p>
                                    </div>
                                </div>
                            </li>
                            <li className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div className="cs-counter simple center">
                                    <div className="cs-text">
                                        <strong className="counter cs-color">2.345</strong>
                                        <span>Pilihan Konten</span>
                                        <p>Jumlah Konten di berbagai kategori.</p>
                                    </div>
                                </div>
                            </li>
                            <li className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div className="cs-counter simple center">
                                    <div className="cs-text">
                                        <strong className="counter cs-color">242.721</strong>
                                        <span>Alumni</span>
                                        <p>Jumlah Alumni yang telah mengikuti pelatihan.</p>
                                    </div>
                                </div>
                            </li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
		       );
    }
}

/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'

import { webStorage, API_BASE } from '../../utils'
import dtiapiClient from '../../dtiapi-client'

import Post from './post'



export default class CourseList extends React.Component {
	constructor() {
		super();
		this.state = {
			title: '',
			lastRetrieved: null,
			hasNext: true,
			contents: []
		}
	}

	componentWillMount() {
		var self = this;
		dtiapiClient.apiGet(`contentsPublic`).then(function (response) {
			// console.log('responsedata:');
			//console.log(response.data);

			self.setState({
				contents: response.data
			});
		}).catch(function (error) {
			console.log(error);
		});
	}


	render() {

		var self = this;

		const { hasNext, contents } = self.state;
		const { location } = self.props;

		let items = self.state.contents.map((post, idx) => (
			<Post key={idx}
				{...post}
				location={location}
			/>
		));

		//var loader = <div className="loader"></div>;

		return (
			<div>
				<div id="advertising-wrap">
				</div>
				<br></br><br></br><br></br>
				<div className="product-showcase-wrap">
					<section className="product-showcase">
						<h3 className="title">Video Pembelajaran Populer</h3>
						<ul className="slide-controls">
							<li>
								<a href="#" className="button prev">
									<svg className="svg-arrow">
										<use xlinkHref="#svg-arrow"></use>
									</svg>
								</a>
							</li>
							<li>
								<a href="#" className="button next">
									<svg className="svg-arrow">
										<use xlinkHref="#svg-arrow"></use>
									</svg>
								</a>
							</li>
						</ul>
						<ul class="product-list grid-v2">	
							{items}
						</ul>
					</section>
				</div>
			</div>
		);
	}
}

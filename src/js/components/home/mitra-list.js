/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'

export default class MitraList extends React.Component {
    constructor() {
        super();
    }
	
	render() {
       
        return (
      	<div className="page-section" style={{marginBottom:'40px'}}>
			<div className="container">
				<div className="row">
					<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div className="cs-fancy-heading" style={{marginBottom:'40px'}}>
							<h6 style={{fontSize:'14px !important',color:'#999 !important',texttransform:'uppercase !important'}}>Mitra-mitra Duta Transformasi Insani</h6>
						</div>
					</div>
					<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<ul className="cs-graduate-slider">
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/dpu.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/wakaf.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/pondok_pesantren.png" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/graduate-logo4.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/graduate-logo5.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/graduate-logo6.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/graduate-logo1.jpg" alt=""/> </figure>
								</div>
							</li>
							<li>
								<div className="cs-media">
									<figure> <img src="assets/images/graduate-logo2.jpg" alt=""/> </figure>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		       );
    }
}

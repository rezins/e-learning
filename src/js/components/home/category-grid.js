/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'

export default class CategoryGrid extends React.Component {
    constructor() {
        super();
    }
	
	render() {
       
        return (
      		<div className="page-section" style={{marginBottom:'45px',marginTop:'0px'}}>
			<div className="container">
				<div className="row">
					<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul className="cs-top-categories">
                            <li><a href="category" style={{background:'#8a9045'}}><i className="icon-uniF1032"></i>Sains</a></li>
                            <li><a href="category" style={{background:'#a88b60'}}><i className="icon-uniF1022"></i>Ekonomi</a></li>
                            <li><a href="category" style={{background:'#3e769a'}}><i className="icon-uniF1052"></i>Teknologi Informasi</a></li>
                            <li><a href="category" style={{background:'#c16622'}}><i className="icon-uniF1012"></i>Statistika</a></li>
                            <li><a href="category#" style={{background:'#896ca9'}}><i className="icon-uniF1042"></i>Design</a></li>
                            <li><a href="category" style={{background:'#dd9d13'}}><i className="icon-uniF1002"></i>Bisnis</a></li>
                        </ul>
						<ul className="cs-top-categories">
							<li><a href="category" style={{background:'#65737e'}}><i className="icon-book2"></i>Penulisan</a></li>
							<li><a href="category" style={{background:'#c594c5'}}><i className="icon-camera2"></i>Fotografi</a></li>
							<li><a href="category" style={{background:'#99c794'}}><i className="icon-leaf"></i>Perkebunan</a></li>
							<li><a href="category" style={{background:'#1b2b34'}}><i className="icon-calendar"></i>Event Organizer</a></li>
							<li><a href="category" style={{background:'#cbe0ff'}}><i className="icon-gears"></i>Otomotif</a></li>
							<li><a href="category" style={{background:'#f0bfb1'}}><i className="icon-heart-o"></i>Kesehatan</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		       );
    }
}

/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import {connect} from 'react-redux'

export default class Event extends React.Component {
    constructor() {
        super();
    }
	
	render() {      
        return (
       	<div className="page-section">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div className="widget cs-text-widget">
						<div className="cs-text" style={{background:'#FFF',padding:'0'}}>
							<h2>Event & Agenda</h2>
							<p>Anda dapat mengikuti berbagai event dan kegiatan kami</p>
							<a href="#" className="cs-bgcolor"><i className="icon-keyboard_arrow_right"></i> Lihat Agenda</a>
						</div>
					</div>
					</div>
					<div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<div className="row">
							<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div className="cs-event left">
									<div className="cs-media">
										<span><strong>Mei</strong>20</span>
									</div>
									<div className="cs-text">
										<em>12:59Pm-03:00Pm</em>
										<h5 style={{marginBottom:'30px'}}><a href="#">Seminar Persiapan Pensiun</a></h5>
										<span><i className="icon-map-marker"></i>Daarut Tauhid Bandung</span>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div className="cs-event left">
									<div className="cs-media">
										<span><strong>Mei</strong>29</span>
									</div>
									<div className="cs-text">
										<em>12:59Pm-03:00Pm</em>
										<h5 style={{marginBottom:'30px'}}><a href="#">Kajian Puasa</a></h5>
										<span><i className="icon-map-marker"></i>Pusdai Bandung</span>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div className="cs-event left">
									<div className="cs-media">
										<span><strong>Juni</strong>02</span>
									</div>
									<div className="cs-text">
										<em>12:59Pm-03:00Pm</em>
										<h5 style={{marginBottom:'30px'}}><a href="#">Bedah Buku.</a></h5>
										<span><i className="icon-map-marker"></i>Auditorium Unjani</span>
									</div>
								</div>
							</div>
                		</div>
					</div>
				</div>
			</div>
		</div>
		       );
    }
}

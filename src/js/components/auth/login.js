/**
 * Created by luthfi on 12/20/16.
 */

import React from 'react'
import { Link } from "react-router"
import { connect } from 'react-redux'
import { hashHistory } from 'react-router'

import { loginUser } from '../../actions/accounts/login-actions'
import Preloader from './preloader'

@connect((store) => {
    return {
        isAuthenticated: store.account.isAuthenticated
    };
})
export default class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            error: null
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        const username = event.target.username.value;
        const password = event.target.password.value;
        const { isAuthenticated, dispatch } = this.props;

        const self = this;

        this.setState({
            isLoading: true
        });

        let credential = {};
        if (this.validateEmail(username))
            credential.email = username;
        else
            credential.identifier = username;

        credential.password = password;

        dispatch(loginUser(credential)).then(() => {
            if (isAuthenticated)
                hashHistory.push('/')
        }).catch((error) => {
            self.setState({
                isLoading: false,
                error: error
            })
        })
    }

    validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    render() {
        const { isAuthenticated } = this.props;
        const { isLoading, error } = this.state;
        let errorMessage = '';
        if (error){
            errorMessage = error.message;
        }

        if (isAuthenticated){
           hashHistory.push('home');
        }

        if (isLoading) {
            return (
                <Preloader />
            );
        }
        return (
            <form className="westeros-form" onSubmit={this.handleSubmit.bind(this)}>
                <label className="rl-label">Your Username</label>
                <input name="username" type="text" placeholder="Email/Username" />
                <label className="rl-label">Your Password</label>
                <input name="password" type="text" placeholder="Password" />
                <input type="checkbox" id="remember" name="remember"></input>
                {errorMessage}
                <label for="remember"><span className="checkbox"><span></span></span>Remember my email and password</label>
                <a href="#" className="fp-msg">Forgot your password? Click here!</a>
                <button className="button login" >Sign in Now!</button>
                <div className="text-center m-t m-b"><small>{errorMessage}</small></div>
            </form>
        );
    }
}
/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import dtiapiClient from '../../dtiapi-client'
import { Link } from 'react-router'
import { webStorage, API_BASE, getImageUrl } from '../../utils'

export default class Menu_semester extends React.Component {
    constructor() {
        super();
    }
	
	render() {
  	    return (
            <aside className="shop-sidebar">

				<svg className="svg-plus sidebar-control">
					<use xlinkHref="#svg-plus"></use>
				</svg>

                <h3 className="title">SEMESTER</h3>
                     <div className="container">
                          <ul>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 1</a>
                              <ul className="dropdown-menu">
                                <li><a href="#/home/coursedetail/599c029541afb7fd34e4a09e">Algoritma Pemrograman (7)</a></li>
                                <li><a href="#">Aplikasi Komputer (5)</a></li>
                                <li><a href="#">Matematika Dasar (5)</a></li>
                                <li><a href="#">Bahasa Inggris 1 (6)</a></li>
                                <li><a href="#">Pendidikan Agama (7)</a></li>
                                <li><a href="#">Bahasa Indonesia (5)</a></li>
                                <li><a href="#">Pendidikan Pancasila (6)</a></li>                                
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 2</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Sturktur Data (8)</a></li>
                                <li><a href="#">Logika Matematika (5)</a></li>
                                <li><a href="#">Aljabar Linier (5)</a></li>
                                <li><a href="#">Pengantar Teknologi Informasi (6)</a></li>
                                <li><a href="#">Organisasi dan Arsitektur Komputer (7)</a></li>
                                <li><a href="#">Bahasa Inggris 2 (5)</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 3</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Pemrograman Objek Dasar(7)</a></li>
                                <li><a href="#">Basis Data (5)</a></li>
                                <li><a href="#">Sistem Operasi (5)</a></li>
                                <li><a href="#">Statistika dan Probabilitas (6)</a></li>
                                <li><a href="#">Matematika Diskrit (7)</a></li>
                                <li><a href="#">Analisa Algoritma dan Pemrograman (5)</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 4</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 5</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 6</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 7</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                              </ul>
                            </li>
                            <li className="dropdown">
                              <input type="checkbox" />
                              <a href="#" data-toggle="dropdown">SEMESTER 8</a>
                              <ul className="dropdown-menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
            </aside>	
	    );
    } 
}

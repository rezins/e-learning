/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'

export default class Algoritma extends React.Component {
    constructor() {
        super();
    }
	
	render() {
  	return (
        <div>
			<h3 className="title">ALGORITMA DAN PEMROGRAMAN</h3>
			<div className="filters">
				<h3 className="subtitle">Items 1 of 7 total</h3>
				<div className="options">
					<form className="westeros-form">
						<label className="select-block" for="show">Show:
							<select name="show" id="show">
								<option value="1">9 Per Page</option>
								<option value="2">20 Per Page</option>
							</select>	
						</label>
						<label className="select-block" for="sort">Sort by:
							<select name="sort" id="sort">
								<option value="1">Price (High to Low)</option>
								<option value="2">Price (Low to High)</option>
							</select>
						</label>
					</form>	
				</div>
            </div>
                <ul className="product-list list">
					<li className="list-item">
						<div className="actions">
							<iframe width="450" height="240" src="https://www.youtube.com/embed/rL8X2mlNHPM"></iframe>
						</div>
						<div className="description" style={{paddingLeft: '250px'}}>
							<div className="clearfix">
								<a href="men-shop.html"><p className="highlighted category">Algoritma dan Pemrograman</p></a>
							</div>
							<div className="clearfix">
								<a href="full-view.html"><h6>BAGIAN 1</h6></a>
							</div>
							<div className="clearfix">
								<p align="justify">Algoritma adalah urutan langkah-langkah logis penyelesaian masalah yang disusun secara sistematis dan logis”. Kata logis merupakan kata kunci dalam algoritma. Langkah-langkah dalam algoritma harus logis dan harus dapat ditentukan bernilai salah atau benar. </p>
								<p className="highlighted current"></p>
							</div>
						</div>
					</li>
					<li className="list-item">
						<div className="actions">
							<iframe width="450" height="240" src="https://www.youtube.com/embed/tpIctyqH29Q"></iframe>
						</div>
						<div className="description" style={{paddingLeft: '250px'}}>
							<div className="clearfix">
								<a href="men-shop.html"><p className="highlighted category">Algoritma dan Pemrograman</p></a>
							</div>
							<div className="clearfix">
								<a href="full-view.html"><h6>BAGIAN 2</h6></a>
							</div>
							<div className="clearfix">
								<p align="justify">Algoritma adalah urutan langkah-langkah logis penyelesaian masalah yang disusun secara sistematis dan logis”. Kata logis merupakan kata kunci dalam algoritma. Langkah-langkah dalam algoritma harus logis dan harus dapat ditentukan bernilai salah atau benar. </p>
								<p className="highlighted current"></p>
							</div>
						</div>
					</li>
					<li className="list-item">
						<div className="actions">
							<iframe width="450" height="240" src="https://www.youtube.com/embed/O5nskjZ_GoI"></iframe>
						</div>
						<div className="description" style={{paddingLeft: '250px'}}>
							<div className="clearfix">
								<a href="men-shop.html"><p className="highlighted category">Algoritma dan Pemrograman</p></a>
							</div>
							<div className="clearfix">
								<a href="full-view.html"><h6>BAGIAN 3</h6></a>
							</div>
							<div className="clearfix">
								<p align="justify">Algoritma adalah urutan langkah-langkah logis penyelesaian masalah yang disusun secara sistematis dan logis”. Kata logis merupakan kata kunci dalam algoritma. Langkah-langkah dalam algoritma harus logis dan harus dapat ditentukan bernilai salah atau benar. </p>
								<p className="highlighted current"></p>
							</div>
						</div>
					</li>
					<li className="list-item">
						<div className="actions">
							<iframe width="450" height="240" src="https://www.youtube.com/embed/rtAlC5J1U40"></iframe>
						</div>
						<div className="description" style={{paddingLeft: '250px'}}>
							<div className="clearfix">
								<a href="men-shop.html"><p className="highlighted category">Algoritma dan Pemrograman</p></a>
							</div>
							<div className="clearfix">
								<a href="full-view.html"><h6>BAGIAN 4</h6></a>
							</div>
							<div className="clearfix">
								<p align="justify">Algoritma adalah urutan langkah-langkah logis penyelesaian masalah yang disusun secara sistematis dan logis”. Kata logis merupakan kata kunci dalam algoritma. Langkah-langkah dalam algoritma harus logis dan harus dapat ditentukan bernilai salah atau benar. </p>
								<p className="highlighted current"></p>
							</div>
						</div>
					</li>
				</ul>
				<ul className="pager">
                <li>
						<a href="#" class="button prev">

							<svg class="svg-arrow">
								<use xlinkHref="#svg-arrow"></use>
							</svg>
						</a>
					</li>
					<li className="selected"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">...</a></li>
					<li><a href="#">17</a></li>
                    <li>
						<a href="#" class="button next">

							<svg class="svg-arrow">
								<use xlinkHref="#svg-arrow"></use>
							</svg>

						</a>
					</li>
				</ul>
		</div>	
	);
  }
}

/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'

import { IndexLink, Link } from 'react-router'
import { hashHistory } from 'react-router'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'

import { webStorage, API_BASE } from '../../utils'
import dtiapiClient from '../../dtiapi-client'

import Post from '../home/post'

export default class Page_informatika extends React.Component {
    constructor() {
        super();
        this.state = {
            title: '',
            lastRetrieved: null,
            hasNext: true,
            contents: []
        }
    }

    componentWillMount() {
       var self = this;
          dtiapiClient.apiGet(`contents?where={"or":[{"category":"599acea841afb7fd34e4a072"}]}`).then(function (response) {
           // console.log('responsedata:');
            //console.log(response.data);
            
           self.setState({
               contents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });
    }

    render() {

        var self = this;

        const { hasNext, contents } = self.state;
        const { location } = self.props;
        
        let items = self.state.contents.map((post,idx) => (
            <Post key={idx}
                {...post}
                location={location}
            />
        ));

        return (
            <div>
                <h3 className="title">INFORMATIKA</h3>
                <div class="filters">
                    <h3 class="subtitle">Items 1 to 10 total</h3>
                    <div className="options">
                        <form className="westeros-form">
                            <label className="select-block" for="show">Show:
								<select name="show" id="show">
                                    <option value="1">9 Per Page</option>
                                    <option value="2">20 Per Page</option>
                                </select>
                                <svg class="svg-arrow select-arrow">
                                    <use xlinkHref="#svg-arrow"></use>
                                </svg>
                            </label>
                            <label className="select-block" for="sort">Sort by:
								<select name="sort" id="sort">
                                    <option value="1">Price (High to Low)</option>
                                    <option value="2">Price (Low to High)</option>
                                </select>

                                <svg className="svg-arrow select-arrow">
                                    <use xlinkHref="#svg-arrow"></use>
                                </svg>

                            </label>
                        </form>

                        <ul className="display">
                            <li class="grid-v2 selected">

                                <svg class="svg-grid">
                                    <use xlinkHref="#svg-grid"></use>
                                </svg>

                            </li>
                            <li className="list">

                                <svg className="svg-list">
                                    <use xlinkHref="#svg-list"></use>
                                </svg>

                            </li >
                        </ul >
                    </div >
                </div >

                <ul className="product-list grid-v2">
                    {items}
                </ul> 
                
                <ul class="pager">
                    <li>
                        <a href="#" class="button prev">
                            <svg class="svg-arrow">
                                <use xlinkHref="#svg-arrow"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="selected"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">24</a></li>
                    <li>
                        <a href="#" class="button next">
                            <svg class="svg-arrow">
                                <use xlinkHref="#svg-arrow"></use>
                            </svg>
                        </a>
                    </li>
                </ul >
            </div >
        );
    }
}

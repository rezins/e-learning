/**
 * Created by Abahsoft on 19/05/2017.
 */

import React from 'react'
import { Link } from "react-router"
import { connect } from 'react-redux'
import { hashHistory } from 'react-router'

export default class If extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <h3 className="title">INFORMATIKA</h3>
                <div class="filters">
                    <h3 class="subtitle">Items 1 to 10 total</h3>
                    <div class="options">
                        <form class="westeros-form">
                            <label class="select-block" for="show">Show:
								<select name="show" id="show">
                                    <option value="1">9 Per Page</option>
                                    <option value="2">20 Per Page</option>
                                </select>
                                <svg class="svg-arrow select-arrow">
                                    <use xlinkHref="#svg-arrow"></use>
                                </svg>
                            </label>
                            <label class="select-block" for="sort">Sort by:
								<select name="sort" id="sort">
                                    <option value="1">Price (High to Low)</option>
                                    <option value="2">Price (Low to High)</option>
                                </select>

                                <svg class="svg-arrow select-arrow">
                                    <use xlinkHref="#svg-arrow"></use>
                                </svg>

                            </label>
                        </form>

                        <ul class="display">
                            <li class="grid-v2 selected">

                                <svg class="svg-grid">
                                    <use xlinkHref="#svg-grid"></use>
                                </svg>

                            </li>
                            <li class="list">

                                <svg class="svg-list">
                                    <use xlinkHref="#svg-list"></use>
                                </svg>

                            </li >
                        </ul >

                    </div >
                </div >

                <ul class="product-list grid-v2">
                    <li className="list-item">
                        <div className="actions">
                            <figure className="liquid">
                                <img src="/assets2/images/items/algoritma.jpg" alt="product1"></img>
                            </figure>
                            <div>
                                <a href="#qv-p1" className="button quick-view" data-effect="mfp-3d-unfold">
                                    <svg className="svg-quickview">
                                        <use xlinkHref="#svg-quickview"></use>
                                    </svg>
                                </a>
                                <div id="qv-p1" className="product-quick-view mfp-with-anim mfp-hide">
                                    <div className="product-pictures">
                                        <h3 className="title">DAFTAR VIDEO</h3>
                                        <ul className="accordion">
                                            <li>
                                                <a href="#">
                                                    <h6>Algoritma Pemrograman</h6>
                                                    <svg className="plus">
                                                        <rect className="vertical" x="3" width="3" height="9" />
                                                        <rect y="3" width="9" height="3" />
                                                    </svg>
                                                </a>
                                                <ul className="sub-items">
                                                    <li>
                                                        <ul>
                                                            <li>
                                                                <a href="#">
                                                                    <svg className="svg-arrow">
                                                                        <use xlinkHref="#svg-arrow"></use>
                                                                    </svg>
                                                                    Bagian 1
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 2
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 3
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 4
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 5
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 6
															</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    Bagian 7
															</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>

                                    </div>

                                    <div className="product-description">
                                        <a href="#"><p className="highlighted category">BY INFORMATIKA</p></a>
                                        <a href="#"><h6>Algoritma Pemrograman</h6></a>

                                        <ul className="rating big">
                                            <li className="filled">

                                                <svg className="svg-star">
                                                    <use xlinkHref="#svg-star"></use>
                                                </svg>

                                            </li>
                                            <li className="filled">

                                                <svg className="svg-star">
                                                    <use xlinkHref="#svg-star"></use>
                                                </svg>

                                            </li>
                                            <li className="filled">
                                                <svg className="svg-star">
                                                    <use xlinkHref="#svg-star"></use>
                                                </svg>
                                            </li>
                                            <li className="filled">
                                                <svg className="svg-star">
                                                    <use xlinkHref="#svg-star"></use>
                                                </svg>

                                            </li>
                                            <li>
                                                <svg className="svg-star">
                                                    <use xlinkHref="#svg-star"></use>
                                                </svg>
                                            </li>
                                        </ul>

                                        <div >
                                            <iframe width="480" height="264" src="https://www.youtube.com/embed/rL8X2mlNHPM">
                                            </iframe>
                                        </div>
                                        <br>
                                            <h5>DESKRIPSI</h5>
                                        </br>

                                        <div className="options" style={{ padding: '0 20 0 0' }}>
                                            <p style={{ align: 'justify' }} >Algoritma adalah urutan langkah-langkah logis penyelesaian masalah yang disusun secara sistematis dan logis”. Kata logis merupakan kata kunci dalam algoritma. Langkah-langkah dalam algoritma harus logis dan harus dapat ditentukan bernilai salah atau benar. </p>
                                        </div>
                                    </div>
                                </div>
                                <a href="full-view.html" className="button full-view">
                                    <svg className="svg-fullview">
                                        <use xlinkHref="#svg-fullview"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div className="description">
                            <div className="clearfix">
                                <a href="#"><p className="highlighted category">by INFORMATIKA</p></a>
                                <ul className="rating">
                                    <li className="filled">
                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li>
                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>
                                    </li>
                                </ul>
                            </div>
                            <div className="clearfix">
                                <a href="#"><h6>ALGORITMA PEMROGRAMAN</h6></a>
                            </div>
                            <div className="clearfix">
                                <p>Pengantar Algoritma</p>
                                <p className="highlighted current">FREE</p>
                            </div>

                            <div className="cart-options">
                                <a href="#" className="button medium wishlist">
                                    <svg className="svg-wishlist">
                                        <use xlinkHref="#svg-wishlist"></use>
                                    </svg>
                                </a>
                                <a href="#" className="button cart-add">
                                    <svg className="svg-plus">
                                        <use xlinkHref="#svg-plus"></use>
                                    </svg>

                                    Lihat Video
							</a>
                            </div>
                        </div>
                    </li>

                    <li className="list-item">

                        <div className="actions">
                            <figure className="liquid">
                                <img src="/assets2/images/items/kecerdasan buatan.jpg" alt="product"></img>
                            </figure>
                            <div>
                                <a href="full-view.html" className="button full-view">
                                    <svg className="svg-fullview">
                                        <use xlinkHref="#svg-fullview"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div className="description">
                            <div className="clearfix">
                                <a href="#"><p className="highlighted category">by INFORMATIKA</p></a>

                                <ul className="rating">
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>
                                    </li>
                                    <li className="filled">
                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li className="filled">

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                    <li>

                                        <svg className="svg-star">
                                            <use xlinkHref="#svg-star"></use>
                                        </svg>

                                    </li>
                                </ul>

                            </div>
                            <div className="clearfix">
                                <a href="#"><h6>Kecerdasan Buatan</h6></a>
                            </div>
                            <div className="clearfix">
                                <p>Pengantar Kecerdasan Buatan</p>
                                <p className="highlighted current"></p>
                            </div>

                            <div className="cart-options">
                                <a href="#" className="button medium wishlist">

                                    <svg className="svg-wishlist">
                                        <use xlinkHref="#svg-wishlist"></use>
                                    </svg>

                                </a>
                                <a href="#" className="button no-stock">

                                    <svg className="svg-plus">
                                        <use xlinkHref="#svg-plus"></use>
                                    </svg>

                                    LOGIN
							</a>
                            </div>

                        </div>

                    </li>
                </ul>

                <ul class="pager">
                    <li>
                        <a href="#" class="button prev">
                            <svg class="svg-arrow">
                                <use xlinkHref="#svg-arrow"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="selected"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">24</a></li>
                    <li>
                        <a href="#" class="button next">

                            <svg class="svg-arrow">
                                <use xlinkHref="#svg-arrow"></use>
                            </svg>

                        </a>
                    </li>
                </ul >
            </div >
        );
    }
}

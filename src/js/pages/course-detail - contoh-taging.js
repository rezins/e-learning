/**
 * Created by AbahSoft on 22/05/2017.
 */
import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import { swipeDetect } from '../utils'
import dtiapiClient from '../dtiapi-client'

{/*
        if(!isAuthenticated)
            hashHistory.push('/home');
*/}


export default class CourseDetail extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            title: '',
            contents: []
        } 

        var konten;  
    }


    componentWillMount() {
       var self = this;
       let idc = this.props.params.id;
          dtiapiClient.apiGetAuth(`contents/`+idc).then(function (response) {
           // console.log('responsedata:');
            //console.log(response.data);
             self.konten = response.data;
             self.setState({
               contents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });
    }



  
    render() {

        if (!this.state.contents.sections) {
            return null;
        }

	   const {account,isAuthenticated, location } = this.props;
       console.log('detail course:')
       console.log(this.state.contents);
       //console.log(this.state.contents.sections);
     	
        return ( 
    <div className="main-section">
      <div className="page-section">
        <div className="container">
          <div className="row">
            <aside className="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div className="widget cs-widget-links">
                    <ul>
                        <li><a href="#cs-about">Program Overview</a></li>
                        <li><a href="#cs-curriculum">Curriculum</a></li>
                        <li><a href="#instrucors">Instructors</a></li>
                        <li><a href="#cs-faqs">FAQs</a></li>
                        <li><a href="#cs-related-post">Related Courses</a></li>
                        <li><a href="#cs-reviews">Reviews</a></li>
                    </ul> 
                    <div className="cs-price">
                        <span><em>Price</em>$20.99<del>$39.99</del></span>
                    </div> 
                    <a href="#" className="cs-button cs-bgcolor">Take this Course</a>            
                </div>
                <div className="widget widget-recent-blog">
                    <ul>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog1.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="cs-price">$49.99</span>
                                <div className="widget-post-title">
                                    <a href="#">How to Design a Logo a Beginners Course</a>
                                </div>
                                <em>By Sarah Jhonson</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog2.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="free">Free</span>
                                <div className="widget-post-title">
                                    <a href="#">Latest Object-Oriented Programming II</a>
                                </div>
                                <em>By Alard William</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog3.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="cs-price">$49.99</span>
                                <div className="widget-post-title">
                                    <a href="#">Learn Color Plending For Photography</a>
                                </div>
                                <em>By Meera Kakkar</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog4.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="free">Free</span>
                                <div className="widget-post-title">
                                    <a href="#">How to Design a Logo a Beginners Course</a>
                                </div>
                                <em>By Sarah Jhonson</em> 
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            <div className="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="cs-about" className="cs-about-courses">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div className="cs-section-title"><h3>{this.state.contents.cont_title} </h3></div>

                                <div className="cs-media"> 
                                    <video width="320" height="240" controls controlsList="noDownload" src={this.state.contents.cont_preview}>
                                   
                                     Your browser does not support the video tag.
                                    </video> 
                                 </div>

                                    <strong>{this.state.contents.cont_title}  </strong>
                                     {this.state.contents.long_description}
                            </div>



                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="cs-courses-overview">
                                    <ul>
                                        <li><i className=" icon-uniF106"></i>className Duration:<span> 1h 41m</span></li>
                                        <li><i className=" icon-play22"></i>Viewers:<span>  620</span></li>
                                        <li><i className="icon-uniF108"></i>Lessons: <span> 23</span></li>
                                        <li><i className="icon-uniF109"></i>Language:<span>English</span></li>
                                        <li><i className="icon-uniF101"></i>Skill level:<span>Beginner</span></li>
                                        <li><i className="icon-uniF117"></i>Students: <span> 50</span></li>
                                        <li><i className=" icon-uniF10A "></i>Certificate: :<span>no</span></li>
                                        <li><i className="icon-uniF104"></i>Assessments: <span>yes</span></li>
                                    </ul>
                                    <a href="#" className="shortlist-btn cs-bgcolor"><i className="icon-plus3"></i>Shortlist</a>
                                </div>
                            </div>

                          
                        </div>
                    </div>
                    <div className="cs-curriculum" id="cs-curriculum">
                        <div className="cs-section-title"><h3>Konten Pelatihan</h3></div>
                       
                        {
                            this.state.contents.sections.map((section,i:string)=>{ 
                                return (
                                   <div>
                                        <h5 className="cs-color">Bagian {i+1}: {section.cont_title}</h5> 
                                        <div id="accordion" className="cs-accordion-list">
                                            
                                             {section.sections.map((video:string,indeks:string)=>{ 

                                                return ( 
                                                    <div className="panel panel-default">
                                                         <div className="panel-heading" role="tablist" id="heading11">
                                                            <h6 className="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href={"#"+i+indeks} aria-expanded="true" aria-controls="collapseOne">
                                                                     <i className="icon-uniF10F cs-color"></i>{video.cont_title}   
                                                                     <span className="cs-questions">Viewed</span>
                                                                    
                                                                     <span className="cs-type-btn private"><i className="icon-lock"></i>Private</span>
                                                                     <span className="cs-type-btn" style={{backgroundColor:'#8ad51f'}}>free</span>
                                                                      <span className="cs-type-btn" style={{backgroundColor:'#ffae00'}}>Play</span>
                                                                </a>    
                                                            </h6>
                                                         </div>
                                                         
                                                         <div id={""+i+indeks} className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                           
                                                            <div className="cs-media"> 
                                                                <video width="320" height="240" controls controlsList="noDownload" src={video.cont_full}>
                                                                    Your browser does not support the video tag.
                                                                </video> 
                                                            </div>
                                                               <p> {video.short_desctiption}</p>
                                                        </div>
                                                    </div>
                                                )
                                             })}


                                        </div>
                                   </div>
                                )
                                
                            })
                        }
                       
                         




                    </div>


                </div>
                <div id="instrucors">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div className="cs-section-title"><h3>Instruktur</h3></div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="cs-team listing">
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/team1.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <h5><a className="cs-color" href="#">Keeth Warson</a></h5>
                                <span>Associate Professor of Anthropology</span>
                                <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation</p>
                                <div className="cs-social-media">
                                    <ul>
                                        <li><a data-original-title="facebook" href="#"><i className="icon-facebook2"></i></a></li>
                                        <li><a data-original-title="pinterest" href="#"><i className="icon-pinterest3"></i></a></li>
                                        <li><a data-original-title="twitter" href="#"><i className="icon-twitter2"></i></a></li>
                                        <li><a data-original-title="google" href="#"><i className="icon-google4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="cs-team listing">
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/team2.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <h5><a className="cs-color" href="#">Peter Parker</a></h5>
                                <span>Associate Professor of Anthropology</span>
                                <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation</p>
                                <div className="cs-social-media">
                                    <ul>
                                        <li><a data-original-title="facebook" href="#"><i className="icon-facebook2"></i></a></li>
                                        <li><a data-original-title="pinterest" href="#"><i className="icon-pinterest3"></i></a></li>
                                        <li><a data-original-title="twitter" href="#"><i className="icon-twitter2"></i></a></li>
                                        <li><a data-original-title="google" href="#"><i className="icon-google4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
                    


                <div id="cs-related-post" className="cs-related-post">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div className="cs-section-title"><h3>Related Course</h3></div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img1.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">Latest Computer Science and Programming</a></h5>
                            </div>
                            <span className="cs-courses-price"><em>$</em>289.99</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img2.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">Basic Time Management Course</a></h5>
                            </div>
                            <span className="cs-free">Free</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img3.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <span className="cs-caption">CC</span>
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">How to Become a Startup Founder</a></h5>
                            </div>
                            <span className="cs-courses-price"><em>$</em>175.99</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


       

    </div>



            

        );
	   
    }
}

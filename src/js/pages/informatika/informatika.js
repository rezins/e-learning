/**
 * Created by AbahSoft on 22/05/2017.
 */
import React from 'react'

import {Link} from 'react-router'
import { swipeDetect } from '../../utils'
import Page_informatika from '../../components/informatika/page_informatika'
import Menu_semester from '../../components/informatika/menu_semester'

{/*
        if(!isAuthenticated)
            hashHistory.push('/home');
*/}

export default class Informatika extends React.Component {
    constructor() {
        super();     
    }

    render() {
        return ( 
            <div id="shop-wrap">
                <section id="shop" className="right expandible-sidebar">
                    <Menu_semester/>
                    <div className="shop-products">
                        <Page_informatika/>
                        
                    </div>
                </section>
            </div>      		
        );	  
    }
}

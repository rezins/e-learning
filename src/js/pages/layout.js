/**
 * Created by luthfi on 12/16/16.
 */
import React from 'react'
import {connect} from 'react-redux'
import { hashHistory } from 'react-router'
import Notifications from 'react-notify-toast'

import Content from '../components/layout/content'
import Header from '../components/layout/header'
import Footer from '../components/layout/footer'

import {getAccount} from '../actions/accounts/get-account-actions'

@connect((store) => {
    return {
        isAuthenticated: store.account.isAuthenticated,
        token: store.account.token,
        userId: store.account.userId,
        account: store.account.account,
		dataUser: store.account.dataUser
    };
})
export default class Layout extends React.Component {
    constructor() {
        super();
    }

	
    componentWillMount() {
        const { dispatch, userId, dataUser} = this.props;
		
      //  dispatch(getAccount(userId));
    }

    componentDidMount() {

    }
	

    render() {	
        const { dispatch, isAuthenticated, userId, account,dataUser } = this.props;	 
        

			{/*}
		  if(!isAuthenticated)
            hashHistory.push('/auth');
			*/}

        return (		    
            <div style={{height:'100%'}}>
			{/*START PAGE */}
			{/*header section */}
               <Header dispatch={dispatch} account={account} isAuthenticated={isAuthenticated} userId={userId} dataUser={dataUser} />
			{/*top navigation section */}
			
			{/*content section */}	
			  <Content>
                  {React.cloneElement(this.props.children, {account: account,isAuthenticated:isAuthenticated})}
              </Content>							
			{/*footer section */}
              <Footer />
			{/*END PAGE */}
		   </div>
        );
    }
}

/**
 * Created by luthfi on 12/20/16.
 */
import React from 'react'
import { IndexLink } from "react-router";
import {connect} from 'react-redux'


@connect((store) => {
    return {
        isAuthenticated: store.account.isAuthenticated
    };
})
export default class Auth extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        $('body')
            .addClass('bg-info dker')
            .css('overflow-y', 'scroll');
    }

    componentWillUnmount() {
        $('body')
            .removeClass('bg-info dker')
            .css('overflow-y', '');
    }

    render() {
        return (
            <div id="register-login-wrap">
            <br></br><br></br><br></br><br></br><br></br><br></br>
                <section id="register-login">
                    <div className="featured-form">
                    <br></br><br></br>
                        <figure className="logo">
                            <img src="/assets2/images/unjani.png" alt="logo"></img>
                            <figcaption>Westeros</figcaption>
                        </figure>
                    </div>
                    <div className="featured-form">
                        <h3>Login To Your Account</h3>
                            {this.props.children}
                    </div>
                </section>
            </div>
           
        );
    }   
}
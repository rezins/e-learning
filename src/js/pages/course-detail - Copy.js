/**
 * Created by AbahSoft on 22/05/2017.
 */
import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import { swipeDetect } from '../utils'
import dtiapiClient from '../dtiapi-client'

{/*
        if(!isAuthenticated)
            hashHistory.push('/home');
*/}


export default class CourseDetail extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            title: '',
            contents: []
        } 

        var konten;  
    }


    componentWillMount() {
       var self = this;
       let idc = this.props.params.id;
          dtiapiClient.apiGetAuth(`contents/`+idc).then(function (response) {
           // console.log('responsedata:');
            //console.log(response.data);
             self.konten = response.data;
             self.setState({
               contents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });
    }



  
    render() {

        if (!this.state.contents.sections) {
            return null;
        }

	   const {account,isAuthenticated, location } = this.props;
       console.log('detail course:')
       //console.log(this.state.contents);
       console.log(this.state.contents.sections);

           
     

                	
        return ( 
    <div className="main-section">
      <div className="page-section">
        <div className="container">
          <div className="row">
            <aside className="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div className="widget cs-widget-links">
                    <ul>
                        <li><a href="#cs-about">Program Overview</a></li>
                        <li><a href="#cs-curriculum">Curriculum</a></li>
                        <li><a href="#instrucors">Instructors</a></li>
                        <li><a href="#cs-faqs">FAQs</a></li>
                        <li><a href="#cs-related-post">Related Courses</a></li>
                        <li><a href="#cs-reviews">Reviews</a></li>
                    </ul> 
                    <div className="cs-price">
                        <span><em>Price</em>$20.99<del>$39.99</del></span>
                    </div> 
                    <a href="#" className="cs-button cs-bgcolor">Take this Course</a>            
                </div>
                <div className="widget widget-recent-blog">
                    <ul>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog1.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="cs-price">$49.99</span>
                                <div className="widget-post-title">
                                    <a href="#">How to Design a Logo a Beginners Course</a>
                                </div>
                                <em>By Sarah Jhonson</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog2.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="free">Free</span>
                                <div className="widget-post-title">
                                    <a href="#">Latest Object-Oriented Programming II</a>
                                </div>
                                <em>By Alard William</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog3.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="cs-price">$49.99</span>
                                <div className="widget-post-title">
                                    <a href="#">Learn Color Plending For Photography</a>
                                </div>
                                <em>By Meera Kakkar</em> 
                            </div>
                        </li>
                        <li>
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/recent-blog4.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <span className="free">Free</span>
                                <div className="widget-post-title">
                                    <a href="#">How to Design a Logo a Beginners Course</a>
                                </div>
                                <em>By Sarah Jhonson</em> 
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            <div className="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="cs-about" className="cs-about-courses">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div className="cs-section-title"><h3>{this.state.contents.cont_title} </h3></div>

                                <div className="cs-media"> 
                                    <video width="320" height="240" controls controlsList="noDownload" src={this.state.contents.cont_preview}>
                                   
                                     Your browser does not support the video tag.
                                    </video> 
                                 </div>

                                    <strong>{this.state.contents.cont_title}  </strong>
                                     {this.state.contents.long_description}
                            </div>



                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="cs-courses-overview">
                                    <ul>
                                        <li><i className=" icon-uniF106"></i>className Duration:<span> 1h 41m</span></li>
                                        <li><i className=" icon-play22"></i>Viewers:<span>  620</span></li>
                                        <li><i className="icon-uniF108"></i>Lessons: <span> 23</span></li>
                                        <li><i className="icon-uniF109"></i>Language:<span>English</span></li>
                                        <li><i className="icon-uniF101"></i>Skill level:<span>Beginner</span></li>
                                        <li><i className="icon-uniF117"></i>Students: <span> 50</span></li>
                                        <li><i className=" icon-uniF10A "></i>Certificate: :<span>no</span></li>
                                        <li><i className="icon-uniF104"></i>Assessments: <span>yes</span></li>
                                    </ul>
                                    <a href="#" className="shortlist-btn cs-bgcolor"><i className="icon-plus3"></i>Shortlist</a>
                                </div>
                            </div>

                          
                        </div>
                    </div>
                    <div className="cs-curriculum" id="cs-curriculum">
                        <div className="cs-section-title"><h3>Konten Pelatihan</h3></div>
                        {
                          this.state.contents.sections.map(function(section) { 
                            return ( 
                               <h5 className="cs-color">{section.cont_title}</h5>
                               )
                            })
                             
                        }

                        {this.state.contents.sections.map((section)=>{ return <h5 className="cs-color">{section.cont_title}</h5> })}
                       
                         

                        <h5 className="cs-color">Section 1: Introduction to Handstands</h5>





                        <div id="accordion" className="cs-accordion-list">
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading11">
                              <h6 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <i className="icon-uniF111 cs-color"></i>Sharon Ross: Character Designer for 2D Animation<span className="cs-type-btn" style={{backgroundColor:'#ffae00'}}>Preview</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading11">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="headingTwo">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  <i className="icon-uniF110 cs-color"></i>Quality of Life: Livability in Future Cities<span className="cs-type-btn" style={{backgroundColor:'#8ad51f'}}>free</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="headingThree">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  <i className="icon-uniF111 cs-color"></i>Quality of Life: Livability in Future Cities <span className="cs-questions">Assignment</span><span className="cs-type-btn private"><i className="icon-lock"></i>Private</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="headingfoure">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefoure" aria-expanded="false" aria-controls="collapsefoure">
                                  <i className="icon-uniF111 cs-color"></i>Quality of Life: Livability in Future Cities <span className="cs-questions">Assignment</span><span className="cs-type-btn private"><i className="icon-lock"></i>Private</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapsefoure" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfoure">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                        </div>
                        <h5 className="cs-color">Section 2: Reference Material, Moodboards and Mind Mapping</h5>
                        <div id="accordion2" className="cs-accordion-list">
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading1">
                              <h6 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion2" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                  <i className="icon-uniF10F cs-color"></i>Sharon Ross: Character Designer for 2D Animation<span className="cs-type-btn" style={{backgroundColor:'#ffae00'}}>Preview</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapse1" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading3">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                  <i className="icon-uniF111 cs-color"></i>Quality of Life: Livability in Future Cities <span className="cs-questions">Assignment</span><span className="cs-type-btn private"><i className="icon-lock"></i>Private</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapse3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading4">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                  <i className="icon-uniF10F cs-color"></i>Quality of Life: Livability in Future Cities <span className="cs-questions">Assignment</span><span className="cs-type-btn private"><i className="icon-lock"></i>Private</span>
                                </a>
                              </h6>
                            </div>
                            <div id="collapse4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                             <p>Lecturer will share some self-development sources for further independent training of course participants. Course will introduce students to the basics of the Structured Query Language (SQL) as well as basic database design for storing data as part of a multi-step data gathering, analysis, and processing effort.</p>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="instrucors">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div className="cs-section-title"><h3>Course Instructor</h3></div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="cs-team listing">
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/team1.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <h5><a className="cs-color" href="#">Keeth Warson</a></h5>
                                <span>Associate Professor of Anthropology</span>
                                <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation</p>
                                <div className="cs-social-media">
                                    <ul>
                                        <li><a data-original-title="facebook" href="#"><i className="icon-facebook2"></i></a></li>
                                        <li><a data-original-title="pinterest" href="#"><i className="icon-pinterest3"></i></a></li>
                                        <li><a data-original-title="twitter" href="#"><i className="icon-twitter2"></i></a></li>
                                        <li><a data-original-title="google" href="#"><i className="icon-google4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="cs-team listing">
                            <div className="cs-media">
                                <figure>
                                    <a href="#"><img alt="#" src="assets/extra-images/team2.jpg"></img></a>
                                </figure>
                            </div>
                            <div className="cs-text">
                                <h5><a className="cs-color" href="#">Peter Parker</a></h5>
                                <span>Associate Professor of Anthropology</span>
                                <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation</p>
                                <div className="cs-social-media">
                                    <ul>
                                        <li><a data-original-title="facebook" href="#"><i className="icon-facebook2"></i></a></li>
                                        <li><a data-original-title="pinterest" href="#"><i className="icon-pinterest3"></i></a></li>
                                        <li><a data-original-title="twitter" href="#"><i className="icon-twitter2"></i></a></li>
                                        <li><a data-original-title="google" href="#"><i className="icon-google4"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="cs-faqs" className="cs-faqs">
                        <div className="cs-section-title"><h3>Frequently Asked Questions</h3></div>
                        <div id="accordion3" className="cs-faqs-list">
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading15">
                              <h6 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion3" href="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                  What is the Capstone Project?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse15" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading15">
                              <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading16">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                  What is the refund policy?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse16" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading17">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                  Can I just enroll in a single course? I'm not interested in the entire Specialization.
                                </a>
                              </h6>
                            </div>
                            <div id="collapse17" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading18">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                  Do I need to take the courses in a specific order?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse18" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading19">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse19" aria-expanded="false" aria-controls="collapse19">
                                  Will I earn university credit for completing the Big Data Specialization?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse19" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading20">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse20" aria-expanded="false" aria-controls="collapse20">
                                  What will I be able to do upon completing the Big Data Specialization?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse20" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading21">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse21" aria-expanded="false" aria-controls="collapse21">
                                  What software will I need to complete the assignments?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse21" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading21">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading22">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse22" aria-expanded="false" aria-controls="collapse22">
                                  What hardware will I need to complete the assignments?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse22" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tablist" id="heading23">
                              <h6 className="panel-title">
                                <a className="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
                                  What connectivity requirements will I need to complete the assignments?
                                </a>
                              </h6>
                            </div>
                            <div id="collapse23" className="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23">
                               <p>Diet and health, human osteology, paleopathology/ epidemiology, human evolution, disease ecology, human adaptation, Stable Isotope Analysis, the Icelandic archaeology, African archaeology. human evolution, disease ecology, human adaptation epidemiology, human evolution, disease ecology, human adaptation,</p>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="cs-related-post" className="cs-related-post">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div className="cs-section-title"><h3>Related Course</h3></div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img1.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">Latest Computer Science and Programming</a></h5>
                            </div>
                            <span className="cs-courses-price"><em>$</em>289.99</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img2.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">Basic Time Management Course</a></h5>
                            </div>
                            <span className="cs-free">Free</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div className="cs-courses courses-grid">
                        <div className="cs-media">
                            <figure><a href="#"><img src="assets/extra-images/course-grid-img3.jpg" alt=""/></a></figure>
                        </div>
                        <div className="cs-text">
                            <span className="cs-caption">CC</span>
                            <div className="cs-rating">
                              <div className="cs-rating-star">
                                <span className="rating-box" style={{width:'100%'}}></span>
                              </div>
                            </div>
                            <div className="cs-post-title">
                              <h5><a href="#">How to Become a Startup Founder</a></h5>
                            </div>
                            <span className="cs-courses-price"><em>$</em>175.99</span>
                            <div className="cs-post-meta">
                              <span>By
                                <a href="#" className="cs-color">James,</a>
                                <a href="#" className="cs-color">Howdson</a>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="page-section" style={{backgroundColor:'#f9f9f9', padding:'80px 0'}}>
        <div className="container">
          <div className="row">
            <div className="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
            <div className="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div className="row">
                <div id="cs-reviews" className="cs-reviews">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div className="cs-section-title"><h3>cOURSE Reviews</h3></div>
                  </div>
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div className="cs-rating-reviews">
                        <div className="cs-review-box">
                            <div className="cs-review-item">
                                <span className="label">5 Stars</span>
                                <span className="cs-item-list">
                                    <span data-width="30%" style={{width: '30%'}}><small>30%</small></span>
                                </span>
                            </div>
                            <div className="cs-review-item">
                                <span className="label">4 Stars</span>
                                <span className="cs-item-list">
                                    <span data-width="55%" style={{width: '55%'}}><small>55%</small></span>
                                </span>
                            </div>
                            <div className="cs-review-item">
                                <span className="label">3 Stars</span>
                                <span className="cs-item-list">
                                    <span data-width="75%" style={{width: '75%'}}><small>75%</small></span>
                                </span>
                            </div>
                            <div className="cs-review-item">
                                <span className="label">2 Stars</span>
                                <span className="cs-item-list">
                                    <span data-width="80%" style={{width: '80%'}}><small>80%</small></span>
                                </span>
                            </div>
                            <div className="cs-review-item">
                                <span className="label">1 Stars</span>
                                <span className="cs-item-list">
                                    <span data-width="95%" style={{width: '95%'}}><small>95%</small></span>
                                </span>
                            </div>
                        </div>
                        <div className="cs-review-summary">
                            <div className="review-average-score">
                                <em className="cs-color">4.8</em>
                                <span className="cs-overall-rating">Overall Ratings</span>
                                <div className="cs-rating">
                                  <div className="cs-rating-star">
                                    <span style={{width:'100%'}} className="rating-box"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>



            

        );
	   
    }
}

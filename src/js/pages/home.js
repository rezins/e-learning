/**
 * Created by AbahSoft on 22/05/2017.
 */
import React from 'react'

import {Link} from 'react-router'
import { swipeDetect } from '../utils'
import BannerTop from '../components/layout/banner'
import CategoryGrid from '../components/home/category-grid'
import CourseList from '../components/home/course-list'
import CourseListIntra from '../components/home/course-list-intra'
import EventList from '../components/home/event-list'
import MemberSummary from '../components/home/member-summary'
import MitraList from '../components/home/mitra-list'

{/*
        if(!isAuthenticated)
            hashHistory.push('/home');
*/}

export default class Home extends React.Component {
    constructor() {
        super();     
    }

    render() {
	   const {account,isAuthenticated, location } = this.props;

       if(!isAuthenticated){            	
        return ( 
            <div>
            <BannerTop />
            <CourseList />					
			</div>
        );
	   }else{ 
	    return ( 
            <div>
            <BannerTop />
			<CourseList />			 
			</div>
        );
	  }
	  
    }
}

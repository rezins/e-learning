/**
 * Created by AbahSoft on 22/05/2017.
 */
import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import { swipeDetect } from '../utils'
import dtiapiClient from '../dtiapi-client'
import Menu_semester from '../components/informatika/menu_semester'
import Page_informatika from '../components/informatika/page_informatika'


{/*
        if(!isAuthenticated)
            hashHistory.push('/home');
*/}


export default class CourseDetail extends React.Component {
    constructor(props) {
        super(props);  
        this.state = {
            title: '',
            contents: [],
            relatedContens: []
        } 

        var konten;  
    }


    componentWillMount() {
       var self = this;
       let idc = this.props.params.id;
       dtiapiClient.apiGetAuth(`contents/`+idc).then(function (response) {
           // console.log('responsedata:');
            //console.log(response.data);
             self.konten = response.data;
             self.setState({
               contents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });

       dtiapiClient.apiGet(`contentsPublic`).then(function (response) {
           // console.log('responsedata:');
            //console.log(response.data);
            
           self.setState({
               relatedContents: response.data
           });
       }).catch(function (error) {
           console.log(error);
       });
    }

    render() {

      if (!this.state.contents.sections) {
        return null;
      }

      const {account,isAuthenticated, location } = this.props;
      console.log('detail course:')
      console.log(this.state.contents);
      //console.log(this.state.contents.sections);
      
      var urlThumbnail:String;
      if(!this.state.contents.thumb) urlThumbnail="assets/images/default.jpg";
      else urlThumbnail = this.state.contents.thumb 

      var urlThumbnail2:String;
      this.state.contents.instructor.map((instruktur,i:string)=>{
        if(!instruktur.profile_picture) urlThumbnail2="assets/images/default.jpg";
        else urlThumbnail2 = instruktur.profile_picture
      }
      ) 

      return ( 
        
        <div id="shop-wrap">
          <section id="shop" className="right expandible-sidebar">
            <aside className="shop-sidebar">
              <svg className="svg-plus sidebar-control">
                <use xlinkHref="#svg-plus"></use>
              </svg>
              <h3 className="title">Learn Course</h3> 
              <div className="product-pictures">
                <div className="actions">
                  <br></br><br></br>
                  <figure>
                    <img height="300px" width="auto" src={urlThumbnail} alt="product-image"></img>
                  </figure>
                </div>
              </div>
              <Link className="button login" to={{pathname:'home/nowcourse/'+this.state.contents.id}} >
                Go To Course
              </Link>
             
            </aside>  
                        
            <div className="shop-products">
              <div className="product-showcase-wrap">
                  <section className="product-showcase">
                    <h3 className="title">DESKRIPSI {this.state.contents.cont_title}</h3>
                    <div className="product-quick-view full builder">
                      <article className="item multiple">
                        <br></br><br></br>
                        <p>{this.state.contents.long_description}</p>
                      </article>  
                    </div>
                    <h3 className="title">MATERI MATA KULIAH {this.state.contents.cont_title}</h3>
                    <div className="container1">
                      <ul>
                        {
                          this.state.contents.sections.map((section,i:string)=>{ 
                            return (
                              <li className="dropdown1">
                                <input type="checkbox" />
                                <a href="#" data-toggle1="dropdown1">{this.state.contents.cont_title} Part {i+1} (SESI KE-{i+1}) </a>
                                <ul className="dropdown-menu1">
                                <ul className="product-list list">
          
                                  { 
                                    section.sections.map((section,index:string)=>{
                                      return(  
                                        <li className="list-item">
                                            <div className="actions">
                                              <iframe width="350" height="240" src={section.cont_full}></iframe>
                                            </div>
                                            <div className="description" style={{paddingLeft: '160px'}}>
                                              <div className="clearfix">
                                                <Link to={{pathname:'home/nowcourse/'+section.catalouge}} ><p className="highlighted category">{section.cont_title}</p></Link>
                                              </div>
                                              <div className="clearfix">
                                                <a href="full-view.html"><h6>BAGIAN KE-{i+1} : {section.total_duration}</h6></a>
                                              </div>
                                              <div className="clearfix">
                                                <p align="justify">{section.short_desctiption}</p>
                                              </div>
                                            </div>
                                          </li>
                                      );
                                    }

                                    )
                                  } 
                                  </ul>                              
                                </ul>
                              </li>
                            );
                          }
                          )
                        }
                        </ul>
                      </div>

                      
              
                    <h3 className="title">Tentang Instruktur</h3>
                    {
                      this.state.contents.instructor.map((instruktur,i:string)=>{ 
                        return (
                          <div className="product-quick-view full builder">
                            <div className="product-pictures">
                              <div className="actions">
                                <br></br><br></br>
                                <figure>
                                  <img height="300px" width="auto" src={urlThumbnail2} alt="profile-picture"></img>
                                </figure>
                              </div>
                            </div>

                            <div className="product-description">
                              <article className="item multiple">
                                <h6>Biodata</h6>
                                <br></br><br></br><br></br>
                                <p>{instruktur.bio}</p>
                              </article>  
                            </div>
                          </div>
                        );
                      }
                      )
                    } 
                  </section>
                </div>
              </div>
            </section>
          </div>             
      );
    }
}
